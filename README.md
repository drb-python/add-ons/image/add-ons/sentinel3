# DRB Sentinel-3 Image AddOn

This addon enrich the `Sentinel-3 Product` topics.
Every measurement files in Sentinel-3 product is considered a possible image.

## Example
```python
zip_node = resolver.create('/path/to/sentinel3.zip')
sen3_node = zip_node[0]
# Retrieve the addon image object corresponding to the product (preview by default)
image = ImageAddon.apply(sen3_node)
# The image name can be specified
image = ImageAddon.apply(safe_node, image_name='Oa01_radiance')
# Retrieve the drb node corresponding to the addon using the default extraction
addon_image_node = image.image_node()
# Retrieve the implementation of the default image
dataset = image.get_impl(xarray.DataArray) 
```