import unittest

import drb.topics.resolver as resolver
import xarray
from drb.drivers.netcdf import DrbNetcdfVariableNode
from drb.image import ImageAddon


class TestSentinel3SRALImage(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.S3_SR_1 = "test/resources/S3A_SR_1_SRA_test.zip"
        cls.S3_SR_2 = "test/resources/S3A_SR_2_LAN_test.zip"
        cls.addon = ImageAddon()

    def test_level1_available_images(self):
        zip_node = resolver.create(self.S3_SR_1)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        # self.assertEqual(len(images), 21)
        self.assertTrue("echo_sample_ind" in images_names)

    def test_level2_available_images(self):
        zip_node = resolver.create(self.S3_SR_2)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        # self.assertEqual(len(images), 1)
        self.assertTrue("dist_coast_20_ku" in images_names)

    def test_level1_images(self):
        zip_node = resolver.create(self.S3_SR_1)
        sen3_node = zip_node[0]
        image_name = "echo_sample_ind"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["measurement.nc"]["root"]["variables"][
            image_name
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level2_images(self):
        zip_node = resolver.create(self.S3_SR_2)
        sen3_node = zip_node[0]
        image_name = "dist_coast_20_ku"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["standard_measurement.nc"]["root"]["variables"][
            "dist_coast_20_ku"
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level2_filter_images(self):
        zip_node = resolver.create(self.S3_SR_2)
        sen3_node = zip_node[0]
        image_name = "dist_coast_20_ku"
        image = self.addon.apply(
            sen3_node, image_name=image_name, measurement="standard"
        )
        addon_image_node = image.image_node()

        image_node = sen3_node["standard_measurement.nc"]["root"]["variables"][
            "dist_coast_20_ku"
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

        image = self.addon.apply(
            sen3_node, image_name=image_name, measurement="enhanced"
        )
        addon_image_node = image.image_node()

        image_node = sen3_node["enhanced_measurement.nc"]["root"]["variables"][
            "dist_coast_20_ku"
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))
