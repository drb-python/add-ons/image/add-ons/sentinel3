import unittest

import drb.topics.resolver as resolver
import xarray
from drb.drivers.netcdf import DrbNetcdfVariableNode
from drb.image import ImageAddon


class TestSentinel3SLSTRImage(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.S3_SL_1 = "test/resources/S3B_SL_1_test.zip"
        cls.S3_SL_2_FRP = "test/resources/S3B_SL_2_FRP_test.zip"
        cls.S3_SL_2_LST = "test/resources/S3B_SL_2_LST_test.zip"
        cls.addon = ImageAddon()

    def test_level1_available_images(self):
        zip_node = resolver.create(self.S3_SL_1)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        self.assertEqual(len(images), 28)
        self.assertTrue("S1_radiance_an" in images_names)
        self.assertTrue("F2_BT_io" in images_names)

    def test_level2_FRP_available_images(self):
        zip_node = resolver.create(self.S3_SL_2_FRP)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        self.assertEqual(len(images), 1)
        self.assertTrue("FRP" in images_names)

    def test_level2_LST_available_images(self):
        zip_node = resolver.create(self.S3_SL_2_LST)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        self.assertEqual(len(images), 1)
        self.assertTrue("LST" in images_names)

    def test_level1_images(self):
        zip_node = resolver.create(self.S3_SL_1)
        sen3_node = zip_node[0]
        image_name = "S1_radiance_ao"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["S1_radiance_ao.nc"]["root"]["variables"][
            image_name
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level1_filter_images(self):
        zip_node = resolver.create(self.S3_SL_1)
        sen3_node = zip_node[0]
        image_name = "F2_BT_in"
        image = self.addon.apply(sen3_node, band="F2", grid="i", view="n")
        addon_image_node = image.image_node()

        image_node = sen3_node["F2_BT_in.nc"]["root"]["variables"][image_name]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level2_FRP_images(self):
        zip_node = resolver.create(self.S3_SL_2_FRP)
        sen3_node = zip_node[0]
        image_name = "FRP"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["FRP_in.nc"]["root"]["variables"]["BT_MIR"]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level2_LST_images(self):
        zip_node = resolver.create(self.S3_SL_2_LST)
        sen3_node = zip_node[0]
        image_name = "LST"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["LST_in.nc"]["root"]["variables"]["LST"]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))
