import unittest

import drb.topics.resolver as resolver
import xarray
from drb.drivers.netcdf import DrbNetcdfVariableNode
from drb.image import ImageAddon


class TestSentinel3SYNImage(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.S3_SY_2 = "test/resources/S3B_SY_2_SYN_test.zip"
        cls.S3_SY_2_VGP = "test/resources/S3A_SY_2_VGP_test.zip"
        cls.S3_SY_2_V10_VG1 = "test/resources/S3B_SY_2_V10_VG1_test.zip"
        cls.addon = ImageAddon()

    def test_level2_available_images(self):
        zip_node = resolver.create(self.S3_SY_2)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        self.assertEqual(len(images), 26)
        self.assertTrue("Syn_Oa01_reflectance" in images_names)
        self.assertTrue("Syn_S1N_reflectance" in images_names)

    def test_level2_VGP_available_images(self):
        zip_node = resolver.create(self.S3_SY_2_VGP)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        self.assertEqual(len(images), 4)
        self.assertTrue("B0" in images_names)
        self.assertTrue("B2" in images_names)
        self.assertTrue("B3" in images_names)
        self.assertTrue("MIR" in images_names)

    def test_level2_V10_VG1_available_images(self):
        zip_node = resolver.create(self.S3_SY_2_V10_VG1)
        sen3_node = zip_node[0]
        images = self.addon.available_images(sen3_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        self.assertEqual(len(images), 5)
        self.assertTrue("B0" in images_names)
        self.assertTrue("B2" in images_names)
        self.assertTrue("B3" in images_names)
        self.assertTrue("MIR" in images_names)
        self.assertTrue("NDVI" in images_names)

    def test_level2_images(self):
        zip_node = resolver.create(self.S3_SY_2)
        sen3_node = zip_node[0]
        image_name = "Syn_Oa01_reflectance"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["Syn_Oa01_reflectance.nc"]["root"]["variables"][
            "SDR_Oa01"
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level2_VGP_images(self):
        zip_node = resolver.create(self.S3_SY_2_VGP)
        sen3_node = zip_node[0]
        image_name = "B0"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["B0.nc"]["root"]["variables"][image_name]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))

    def test_level2_V10_VG1_images(self):
        zip_node = resolver.create(self.S3_SY_2_V10_VG1)
        sen3_node = zip_node[0]
        image_name = "NDVI"
        image = self.addon.apply(sen3_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = sen3_node["NDVI.nc"]["root"]["variables"]["NDVI"]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))
